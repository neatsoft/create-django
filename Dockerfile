FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /app

RUN pip install --no-cache-dir django

COPY ./entrypoint /

ENTRYPOINT ["/entrypoint"]
