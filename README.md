# create-django

## Build docker image and push it to the dockerhub

    docker build -t neatsoft/create-django .
    docker login
    docker push neatsoft/create-django

## Create new project

    docker run -it -u `id -u`:`id -g` -v `pwd`:/app neatsoft/create-django <project_name>
